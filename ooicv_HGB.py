import numpy as np
from osgeo import gdal
from skimage import exposure
from skimage.segmentation import quickshift
from skimage.segmentation import slic
import time
import geopandas as gpd
import pandas as pd
from osgeo import ogr
from sklearn.ensemble import HistGradientBoostingClassifier
from sklearn.metrics import accuracy_score

gdal.UseExceptions()

# Load and preprocess the image
naip_fn = '/home/vitor/Downloads/machine/sentinel/Sentinel_sul_new_complete.tif'
naip_ds = gdal.Open(naip_fn)
nbands = naip_ds.RasterCount
ysize = naip_ds.RasterYSize
xsize = naip_ds.RasterXSize
band_data = []

print('bands:', nbands, 'rows:', ysize, 'columns:', xsize)

# Add band information to the array
for i in range(1, nbands + 1):
    band = naip_ds.GetRasterBand(i).ReadAsArray()
    band_data.append(band)
band_data = np.dstack(band_data)

# Scale image values from 0.0 to 1.0
img = band_data

seg_start = time.time()

# Perform image segmentation
#segments = quickshift(img, ratio=0.6, max_dist=4, sigma=0.7, convert2lab=False)
segments = slic(img,compactness=0.4,n_segments=20000)
# segments = slic(img, n_segments=60000, compactness=0.1)

print('Segments completed in', time.time() - seg_start, 'seconds')

# Save segments to raster
segments_fn = '/home/vitor/Downloads/machine/sentinel/Sentinel_segmented_.tif'
driverTiff = gdal.GetDriverByName('GTiff')
segments_ds = driverTiff.Create(segments_fn, naip_ds.RasterXSize, naip_ds.RasterYSize,
                                1, gdal.GDT_UInt32)
segments_ds.SetGeoTransform(naip_ds.GetGeoTransform())
segments_ds.SetProjection(naip_ds.GetProjectionRef())
segments_ds.GetRasterBand(1).WriteArray(segments)
segments_ds = None

# Load the training data and assign labels to segments
gdf = gpd.read_file('/home/vitor/Downloads/machine/sentinel/train_test.shp')
class_names = gdf['label'].unique()
class_numbers = np.arange(class_names.size) + 1
df = pd.DataFrame({'label': class_names, 'number': class_numbers})
df.to_csv('/home/vitor/Downloads/machine/sentinel/class_lookup.csv')
gdf['number'] = gdf['label'].map(dict(zip(class_names, class_numbers)))

# Split the truth data into training and test data sets
gdf_train = gdf.sample(frac=0.7)
gdf_test = gdf.drop(gdf_train.index)

train_fn = '/home/vitor/Downloads/machine/sentinel/training_data.shp'
test_fn = '/home/vitor/Downloads/machine/sentinel/test_data.shp'

gdf_train.to_file(train_fn)
gdf_test.to_file(test_fn)

# Rasterize the training data
train_ds = ogr.Open(train_fn)
lyr = train_ds.GetLayer()
driver = gdal.GetDriverByName('MEM')
target_ds = driver.Create('', naip_ds.RasterXSize, naip_ds.RasterYSize, 1, gdal.GDT_Byte)
target_ds.SetGeoTransform(naip_ds.GetGeoTransform())
target_ds.SetProjection(naip_ds.GetProjection())
options = ['ATTRIBUTE=number']
gdal.RasterizeLayer(target_ds, [1], lyr, options=options)
ground_truth = target_ds.GetRasterBand(1).ReadAsArray()

seg_start = time.time()

# Rescale image values from 0.0 to 1.0
img = exposure.rescale_intensity(img)


objects = []
segment_numbers = np.unique(segments)
for segment_number in segment_numbers:
    segment_pixels = img[segments == segment_number]
    object_features = segment_pixels.mean(axis=0)  # Use mean value as a feature
    objects.append(object_features)

# Associate training data. Objects to labels
training_objects = []
training_labels = []
for class_number in class_numbers:
    class_train_object = [v for i, v in enumerate(objects) if segment_numbers[i] in segments[ground_truth == class_number]]
    training_labels += [class_number] * len(class_train_object)
    training_objects += class_train_object

# Set the classifier to be used
classifier = HistGradientBoostingClassifier()

# Perform the training and classification
try:
    # Training step
    classifier.fit(training_objects, training_labels, None)
    # Classification step for training data
    predicted = classifier.predict(objects)
    # Reshape predicted array to match the size of the segmented raster
    predicted_reshaped = np.zeros(segments.shape)
    for i, segment_number in enumerate(segment_numbers):
        predicted_reshaped[segments == segment_number] = predicted[i]

    # Save predicted image to file for training
    classified_fn = '/home/vitor/Downloads/machine/sentinel/Sentinel_classified.tif'
    classified_ds = driverTiff.Create(classified_fn, xsize, ysize,
                                      1, gdal.GDT_Byte)
    classified_ds.SetGeoTransform(naip_ds.GetGeoTransform())
    classified_ds.SetProjection(naip_ds.GetProjectionRef())
    classified_ds.GetRasterBand(1).WriteArray(predicted_reshaped)
    classified_ds = None
    print('Training and Classification completed in', time.time() - seg_start, 'seconds')

except Exception as e:
    print("An error occurred during training and classification:", str(e))
    # Handle the error




# Load the test data and assign labels to segments
gdf_test = gpd.read_file('/home/vitor/Downloads/machine/sentinel/test_data.shp')
gdf_test['number'] = gdf_test['label'].map(dict(zip(class_names, class_numbers)))

# Rasterize the test data
test_ds = ogr.Open(test_fn)
test_lyr = test_ds.GetLayer()
test_target_ds = driver.Create('', naip_ds.RasterXSize, naip_ds.RasterYSize, 1, gdal.GDT_UInt16)
test_target_ds.SetGeoTransform(naip_ds.GetGeoTransform())
test_target_ds.SetProjection(naip_ds.GetProjection())
gdal.RasterizeLayer(test_target_ds, [1], test_lyr, options=options)
test_ground_truth = test_target_ds.GetRasterBand(1).ReadAsArray()

# Feature Extraction for Testing
test_objects = []
for segment_number in segment_numbers:
    segment_pixels = img[segments == segment_number]
    object_features = segment_pixels.mean(axis=0)  # Use mean value as a feature
    test_objects.append(object_features)

# Classification step for testing data
test_predicted = classifier.predict(test_objects)

# Reshape predicted array to match the size of the segmented raster
test_predicted_reshaped = np.zeros(segments.shape)
for i, segment_number in enumerate(segment_numbers):
    test_predicted_reshaped[segments == segment_number] = test_predicted[i]

# Compare the predicted labels with the ground truth labels for testing data
test_accuracy = accuracy_score(test_ground_truth.ravel(), test_predicted_reshaped.ravel())
print('Test Accuracy:', test_accuracy)