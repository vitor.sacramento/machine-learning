import numpy as np
from osgeo import gdal
import geopandas as gpd
import pandas as pd
from osgeo import ogr
import time
from sklearn.ensemble import RandomForestClassifier
from skimage import exposure
from skimage.segmentation import slic

gdal.UseExceptions()

# Load and preprocess the image
naip_fn = '/home/vitor/Downloads/machine/sentinel/Sentinel_Image_NEW_NDVI_Normalized.tif'
naip_ds = gdal.Open(naip_fn)
nbands = naip_ds.RasterCount
ysize = naip_ds.RasterYSize
xsize = naip_ds.RasterXSize
band_data = []

print('bands:', nbands, 'rows:', ysize, 'columns:', xsize)

# Add band information to the array
for i in range(1, nbands + 1):
    band = naip_ds.GetRasterBand(i).ReadAsArray()
    band_data.append(band)
band_data = np.dstack(band_data)

# Scale image values from 0.0 to 1.0
img = exposure.rescale_intensity(band_data)

seg_start = time.time()

# Perform image segmentation
# segments = quickshift(img, convert2lab=False)
# segments = quickshift(img, ratio=0.55, convert2lab=False)
# segments = quickshift(img, ratio=0.6, max_dist=5, convert2lab=False)
# segments = slic(img, n_segments=60000, compactness=0.1)
# segments = slic(img, n_segments=500000, compactness=0.01)
segments = slic(img, n_segments=70000, compactness=0.1)

print('Segments completed in', time.time() - seg_start, 'seconds')

# Save segments to raster
segments_fn = '/home/vitor/Downloads/machine/sentinel/new_test_segmented.tif'
driverTiff = gdal.GetDriverByName('GTiff')
segments_ds = driverTiff.Create(segments_fn, xsize, ysize,
                                1, gdal.GDT_Float32)
segments_ds.SetGeoTransform(naip_ds.GetGeoTransform())
segments_ds.SetProjection(naip_ds.GetProjectionRef())
segments_ds.GetRasterBand(1).WriteArray(segments)
segments_ds = None

# Load the training data and assign labels to segments
gdf = gpd.read_file('/home/vitor/Downloads/machine/sentinel/train_test.shp')
class_names = gdf['label'].unique()
class_ids = np.arange(class_names.size) + 1
df = pd.DataFrame({'label': class_names, 'id': class_ids})
df.to_csv('/home/vitor/Downloads/machine/sentinel/class_lookup.csv')
gdf['id'] = gdf['label'].map(dict(zip(class_names, class_ids)))

# Split the truth data into training and test data sets
gdf_train = gdf.sample(frac=0.7)
gdf_test = gdf.drop(gdf_train.index)
# save training and test data to shapefiles
gdf_train.to_file('/home/vitor/Downloads/machine/sentinel/train_data.shp')
gdf_test.to_file('/home/vitor/Downloads/machine/sentinel/test_data.shp')

# Rasterize the training data
train_fn = '/home/vitor/Downloads/machine/sentinel/train_data.shp'
train_ds = ogr.Open(train_fn)
lyr = train_ds.GetLayer()
driver = gdal.GetDriverByName('MEM')
target_ds = driver.Create('', xsize, ysize, 1, gdal.GDT_UInt16)
target_ds.SetGeoTransform(naip_ds.GetGeoTransform())
target_ds.SetProjection(naip_ds.GetProjection())
options = ['ATTRIBUTE=id']
gdal.RasterizeLayer(target_ds, [1], lyr, options=options)
ground_truth = target_ds.GetRasterBand(1).ReadAsArray()

seg_start = time.time()

# Extract object features
objects = []
segment_ids = np.unique(segments)
for segment_id in segment_ids:
    segment_pixels = img[segments == segment_id]
    object_features = segment_pixels.mean(axis=0)  # Use mean value as a feature
    objects.append(object_features)

# Associate training data. Objects to labels
training_objects = []
training_labels = []
for class_id in class_ids:
    class_train_object = [v for i, v in enumerate(objects) if segment_ids[i] in segments[ground_truth == class_id]]
    training_labels += [class_id] * len(class_train_object)
    training_objects += class_train_object

# Set the classifier to be used
classifier = RandomForestClassifier()

# Perform the training and classification
try:
    # Training step
    classifier.fit(training_objects, training_labels)
    # Classification step
    predicted = classifier.predict(objects)
    # Reshape predicted array to match the size of the segmented raster
    predicted_reshaped = np.zeros(segments.shape)
    for i, segment_id in enumerate(segment_ids):
        predicted_reshaped[segments == segment_id] = predicted[i]

    # Save predicted image to file
    classified_fn = '/home/vitor/Downloads/machine/sentinel/new_test_classified.tif'
    classified_ds = driverTiff.Create(classified_fn, xsize, ysize,
                                      1, gdal.GDT_Byte)
    classified_ds.SetGeoTransform(naip_ds.GetGeoTransform())
    classified_ds.SetProjection(naip_ds.GetProjectionRef())
    classified_ds.GetRasterBand(1).WriteArray(predicted_reshaped)
    classified_ds = None
    print('Classification completed in', time.time() - seg_start, 'seconds')

except Exception as e:
    print("An error occurred during classification:", str(e))
    # Handle the error