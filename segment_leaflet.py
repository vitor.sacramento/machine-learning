import os
import torch
from samgeo import SamGeo, tms_to_geotiff
from osgeo import ogr
from tkinter.filedialog import askopenfilename
import time

# load shapefile
shapefile_path = askopenfilename(
    title="Select your ROI",
    filetypes=[('Shapefile', '*.shp')]
)

driver = ogr.GetDriverByName('ESRI Shapefile')
dataset = driver.Open(shapefile_path)
layer = dataset.GetLayer()

# acquire the epsg from the shapefile
EPSG = int(layer.GetSpatialRef().GetAttrValue('AUTHORITY',1))

# get bounding box coordinates
if EPSG == 4326 or  EPSG == 4674:
    extent = layer.GetExtent()
    minx, maxx, miny, maxy = extent
else:
    print('Shapefile not in WGS 84 Geographic(EPSG:4326)')
    exit()

# print bounding box coordinates
print(f"Bounding box coordinates: ({minx}, {miny}, {maxx}, {maxy})")

bbox=[minx,miny,maxx,maxy]

image = os.path.join(os.path.dirname(shapefile_path),'leaflet.tiff')

if not os.path.exists(os.path.dirname(image)):
    os.makedirs(os.path.dirname(image))

tms_to_geotiff(output=image, bbox=bbox, zoom=14, source='Satellite')

# Track the start time
start_time = time.time()

checkpoint = os.path.join(os.path.expanduser('~'), 'Documentos','SAMGeo','checkpoints','sam_vit_h_4b8939.pth')

torch.backends.cuda.split_kernel_size = 2000

device = 'cuda' if torch.cuda.is_available() else 'cpu'

# Availble sam_kwargs:

# points_per_side: Optional[int] = 32,
# points_per_batch: int = 64,
# pred_iou_thresh: float = 0.88,
# stability_score_thresh: float = 0.95,
# stability_score_offset: float = 1.0,
# box_nms_thresh: float = 0.7,
# crop_n_layers: int = 0,
# crop_nms_thresh: float = 0.7,
# crop_overlap_ratio: float = 512 / 1500,
# crop_n_points_downscale_factor: int = 1,
# point_grids: Optional[List[np.ndarray]] = None,
# min_mask_region_area: int = 0,
# output_mode: str = "binary_mask"

sam_kwargs = {'stability_score_offset':0.8}

sam = SamGeo(
    model_type="vit_h",
    checkpoint=checkpoint,
    sam_kwargs=None,
)

mask = "segment.tif"
sam.generate(
    image, mask, batch=True, foreground=True, erosion_kernel=(3, 3), mask_multiplier=255
)

vector = os.path.join(os.path.dirname(shapefile_path),'segment.gpkg')
sam.tiff_to_gpkg(mask, vector, simplify_tolerance=None)

# Calculate the elapsed time
elapsed_time = time.time() - start_time
print(f"Elapsed time: {elapsed_time:.4f} seconds")