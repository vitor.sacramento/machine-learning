import os
import torch
from samgeo import SamGeo
from tkinter.filedialog import askopenfilename

# load shapefile
image = askopenfilename(
    title="Select your TIFF",
    filetypes=[('GeoTiff', '*.tif')]
)

checkpoint = askopenfilename(
    title="Select your checkpoint",
    filetypes=[('SAM_Checkpoint', '*.pth')]
)

device = 'cuda' if torch.cuda.is_available() else 'cpu'

# Availble sam_kwargs:

# points_per_side: Optional[int] = 32,
# points_per_batch: int = 64,
# pred_iou_thresh: float = 0.88,
# stability_score_thresh: float = 0.95,
# stability_score_offset: float = 1.0,
# box_nms_thresh: float = 0.7,
# crop_n_layers: int = 0,
# crop_nms_thresh: float = 0.7,
# crop_overlap_ratio: float = 512 / 1500,
# crop_n_points_downscale_factor: int = 1,
# point_grids: Optional[List[np.ndarray]] = None,
# min_mask_region_area: int = 0,
# output_mode: str = "binary_mask"

sam_kwargs = {'stability_score_thresh':0.6,'min_mask_region_area':0}

sam = SamGeo(
    model_type="vit_h",
    checkpoint=checkpoint,
    sam_kwargs=None,
)

mask = os.path.join(os.path.dirname(image),os.path.splitext(os.path.basename(image))[0])+"_segment.tif"

sam.generate(
    image, mask, batch=True, foreground=True, erosion_kernel=(5, 5), mask_multiplier=1
)

vector = os.path.join(os.path.dirname(image),os.path.splitext(os.path.basename(image))[0])+"_segment.gpkg"

sam.tiff_to_gpkg(mask, vector, simplify_tolerance=None)