from osgeo import gdal,ogr
import numpy as np
import pandas as pd
import geopandas as gpd
from skimage.segmentation import slic
from skimage import exposure
from sklearn.ensemble import HistGradientBoostingClassifier
from sklearn.metrics import confusion_matrix, accuracy_score, cohen_kappa_score, classification_report
import time
import traceback

gdal.UseExceptions()

# Load and preprocess the image
naip_fn = '/home/vitor/Downloads/machine/sentinel/Imagem_classificacao_orientada_objetos_Sentinel_Image_NDVI_NDBI_SAVI_EVI.tif'
naip_ds = gdal.Open(naip_fn)
nbands = naip_ds.RasterCount
ysize = naip_ds.RasterYSize
xsize = naip_ds.RasterXSize
band_data = []

print('bands:', nbands, 'rows:', ysize, 'columns:', xsize)

# Add band information to the array
for i in range(1, nbands + 1):
    band = naip_ds.GetRasterBand(i).ReadAsArray()
    band_data.append(band)
band_data = np.dstack(band_data)

# Scale image values from 0.0 to 1.0
img = band_data

seg_start = time.time()

# Perform image segmentation
#segments = quickshift(img, ratio=0.6, max_dist=4, sigma=0.7, convert2lab=False)
segments = slic(img,compactness=0.4,n_segments=120000)
# segments = slic(img, n_segments=60000, compactness=0.1)

print('Segments completed in', time.time() - seg_start, 'seconds')

# Save segments to raster
segments_fn = '/home/vitor/Downloads/machine/sentinel/Sentinel_segmented_.tif'
driverTiff = gdal.GetDriverByName('GTiff')
segments_ds = driverTiff.Create(segments_fn, naip_ds.RasterXSize, naip_ds.RasterYSize,
                                1, gdal.GDT_UInt32)
segments_ds.SetGeoTransform(naip_ds.GetGeoTransform())
segments_ds.SetProjection(naip_ds.GetProjectionRef())
segments_ds.GetRasterBand(1).WriteArray(segments)
segments_ds = None

# Load the training data and assign labels to segments
gdf = gpd.read_file('/home/vitor/Downloads/machine/sentinel/train_test.shp')
class_names = gdf['label'].unique()
class_numbers = np.arange(class_names.size) + 1
df = pd.DataFrame({'label': class_names, 'number': class_numbers})
df.to_csv('/home/vitor/Downloads/machine/sentinel/class_lookup.csv')
gdf['number'] = gdf['label'].map(dict(zip(class_names, class_numbers)))

# Split the truth data into training and test data sets
gdf_train = gdf.sample(frac=0.7)
gdf_test = gdf.drop(gdf_train.index)

train_fn = '/home/vitor/Downloads/machine/sentinel/training_data.shp'
test_fn = '/home/vitor/Downloads/machine/sentinel/test_data.shp'

gdf_train.to_file(train_fn)
gdf_test.to_file(test_fn)

# Rasterize the training data
train_ds = ogr.Open(train_fn)
lyr = train_ds.GetLayer()
driver = gdal.GetDriverByName('MEM')
target_ds = driver.Create('', naip_ds.RasterXSize, naip_ds.RasterYSize, 1, gdal.GDT_Byte)
target_ds.SetGeoTransform(naip_ds.GetGeoTransform())
target_ds.SetProjection(naip_ds.GetProjection())
options = ['ATTRIBUTE=number']
gdal.RasterizeLayer(target_ds, [1], lyr, options=options)
ground_truth = target_ds.GetRasterBand(1).ReadAsArray()

seg_start = time.time()

# Rescale image values from 0.0 to 1.0
img = exposure.rescale_intensity(img)

objects = []
segment_numbers = np.unique(segments)
for segment_number in segment_numbers:
    segment_pixels = img[segments == segment_number]
    object_features = segment_pixels.mean(axis=0)  # Use mean value as a feature
    objects.append(object_features)

# Associate training data. Objects to labels
training_objects = []
training_labels = []
for class_number in class_numbers:
    class_train_object = [v for i, v in enumerate(objects) if segment_numbers[i] in segments[ground_truth == class_number]]
    training_labels += [class_number] * len(class_train_object)
    training_objects += class_train_object

# Set the classifier to be used
classifier = HistGradientBoostingClassifier()

# Perform the training and classification
try:
    # Training step
    classifier.fit(training_objects, training_labels, None)
    # Classification step for training data
    predicted = classifier.predict(objects)
    # Reshape predicted array to match the size of the segmented raster
    predicted_reshaped = np.zeros(segments.shape)
    for i, segment_number in enumerate(segment_numbers):
        predicted_reshaped[segments == segment_number] = predicted[i]
    
    # Save the classified raster to a file
    classified_fn = '/home/vitor/Downloads/machine/sentinel/Sentinel_classified_.tif'
    driverTiff = gdal.GetDriverByName('GTiff')
    classified_ds = driverTiff.Create(classified_fn, naip_ds.RasterXSize, naip_ds.RasterYSize, 1, gdal.GDT_UInt16)
    classified_ds.SetGeoTransform(naip_ds.GetGeoTransform())
    classified_ds.SetProjection(naip_ds.GetProjectionRef())
    classified_ds.GetRasterBand(1).WriteArray(predicted_reshaped)
    classified_ds = None
    
    # Rasterize the test data
    test_ds = ogr.Open(test_fn)
    lyr = test_ds.GetLayer()
    driver = gdal.GetDriverByName('MEM')
    target_ds = driver.Create('', naip_ds.RasterXSize, naip_ds.RasterYSize, 1, gdal.GDT_UInt16)
    target_ds.SetGeoTransform(naip_ds.GetGeoTransform())
    target_ds.SetProjection(naip_ds.GetProjection())
    options = ['ATTRIBUTE=number']
    gdal.RasterizeLayer(target_ds, [1], lyr, options=options)
    test_truth = target_ds.GetRasterBand(1).ReadAsArray()

    # Extract the test data objects and labels
    test_objects = []
    test_labels = []
    for class_number in class_numbers:
        class_test_object = [v for i, v in enumerate(objects) if segment_numbers[i] in segments[test_truth == class_number]]
        test_labels += [class_number] * len(class_test_object)
        test_objects += class_test_object

    # Classification step for test data
    test_predicted = classifier.predict(test_objects)

    # Compute the confusion matrix
    cm = confusion_matrix(test_labels, test_predicted)

    # Print the confusion matrix
    print('Confusion matrix:')
    print(cm)

    # Calculate the overall accuracy
    oa = accuracy_score(test_labels, test_predicted)

    # Print the overall accuracy
    print('Overall accuracy:', oa)

    # Calculate the producer's accuracy
    pa = np.diag(cm) / np.sum(cm, axis=1)

    # Print the producer's accuracy
    print('Producer\'s accuracy:', pa)

    # Calculate the user's accuracy
    ua = np.diag(cm)
except Exception as e:
    traceback.print_exc()