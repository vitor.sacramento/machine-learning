import torch
from segment_anything import sam_model_registry
import cv2
from segment_anything import SamAutomaticMaskGenerator
import supervision as sv
from tkinter.filedialog import askopenfilename
import os
import time

# Set the maximum split size in megabytes
os.environ["PYTORCH_CUDA_ALLOC_CONF"] = "max_split_size_mb:300"

input_file = askopenfilename(title='Selecione o dado de entrada')

# Track the start time
start_time = time.time()

DEVICE = torch.device('cuda:0' if torch.cuda.is_available() and torch.cuda.get_device_capability()[0] >= 7 else 'cpu')
MODEL_TYPE = "vit_b"

sam = sam_model_registry[MODEL_TYPE](checkpoint=r'/home/vitor/Documentos/sam_vit_b_01ec64.pth')
sam.to(device='cpu')

mask_generator = SamAutomaticMaskGenerator(sam)

image_bgr = cv2.imread(input_file)
image_rgb = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2RGB)

# Convert the image_rgb to a tensor
image_rgb = torch.from_numpy(image_rgb)

# Reduce the batch size by splitting the image into smaller chunks
image_chunks = torch.chunk(image_rgb, chunks=4, dim=0) # Adjust the number of chunks according to your GPU memory

# Initialize an empty list to store the results for each chunk
results = []

# Loop over the image chunks and generate masks for each one
for image_chunk in image_chunks:
    # Permute the dimensions of the image_chunk to make the channels the first dimension
    image_chunk = image_chunk.permute(2, 0, 1)
    result = mask_generator.generate(image_chunk)
    results.append(result)

# Concatenate the results back into a single tensor
result = torch.cat(results, dim=0)

mask_annotator = sv.MaskAnnotator()
detections = sv.Detections.from_sam(result)
annotated_image = mask_annotator.annotate(image_bgr, detections)

cv2.imwrite(os.path.join(os.path.dirname(input_file),os.path.splitext(os.path.basename(input_file))[0])+'annotated.tif', annotated_image)

# Calculate the elapsed time
elapsed_time = time.time() - start_time
print(f"Elapsed time: {elapsed_time:.4f} seconds")