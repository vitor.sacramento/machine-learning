from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename
import numpy as np
import rasterio
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score,confusion_matrix
import geopandas as gpd
import time

def HistGradientBoost(image_file,shapefile,output_file):

    satellite_image = rasterio.open(image_file)

    start_time = time.time()

    # Leia a imagem a converta para um array numpy
    image_data = satellite_image.read()
    image_data = np.moveaxis(image_data, 0, -1)
    height, width, bands = image_data.shape

    # Obtenha o valor dos pixels referente aos dados de treinamento
    training_samples = []
    training_labels = []

    for index, sample in shapefile.iterrows():
        geom = sample.geometry
        coords = rasterio.transform.rowcol(satellite_image.transform, geom.centroid.x, geom.centroid.y)
        x, y = coords[1], coords[0]
        if 0 <= x < width and 0 <= y < height:
            pixel_values = image_data[y, x]
            training_samples.append(pixel_values)
            training_labels.append(sample['id'])

    # Divida a amostra em teste e treino
    X_train, X_test, y_train, y_test = train_test_split(training_samples, training_labels, test_size=0.2)

    # Treine seu modelo
    classifier = RandomForestClassifier()
    classifier.fit(X_train, y_train)

    # Classifique a sua imagem
    image_data_reshaped = image_data.reshape(height * width, bands)
    predicted_labels = classifier.predict(image_data_reshaped)

    # Volte os pixels classificados para um array multiplo
    predicted_labels = predicted_labels.reshape((height, width))

    # Treine o modelo com as amostras de teste
    train_predictions = classifier.predict(X_test)

    # Calcule a acurácia do modelo
    accuracy = accuracy_score(y_test, train_predictions)

    # Calcule a matriz de confusão
    cm = confusion_matrix(y_test, train_predictions)

    # Calcule a acurácia por classe
    pa = np.diag(cm) / np.sum(cm, axis=1)

    print('Confusion matrix: ')
    print(cm)

    print('Producer\'s accuracy:', pa)

    print("Overall Accuracy:", accuracy)

    # Salve a imagem
    with rasterio.open(output_file, 'w', driver='GTiff', height=height, width=width, count=1, dtype=predicted_labels.dtype,
                    crs=satellite_image.crs, transform=satellite_image.transform) as output_image:
        output_image.write(predicted_labels, 1)

    print("Classification completed in", time.time()-start_time," seconds",", classified image saved to:", output_file)

if __name__=='__main__':
    # Abra a imagem a ser classificada
    image_file = askopenfilename(title='Selecione o Raster de entrada')

    # Abra o shape com o treinamento
    shapefile = gpd.read_file(askopenfilename(title='Selecione o Shape de treinamento de entrada'))

    # Escolha o arquivo a ser salvo
    output_file = asksaveasfilename(title='Selecione o Raster de saída')

    HistGradientBoost(image_file,shapefile,output_file)