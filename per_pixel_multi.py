from tkinter.filedialog import askopenfilename
from tkinter.filedialog import asksaveasfilename
import numpy as np
import rasterio
from sklearn.ensemble import HistGradientBoostingClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix
import geopandas as gpd
import time
import multiprocessing

# Função para classificar um chunk da imagem
def classify_chunk(chunk, classifier):
    chunk_shape = chunk.shape
    chunk_reshaped = chunk.reshape(chunk_shape[0] * chunk_shape[1], chunk_shape[2])
    predicted_chunk = classifier.predict(chunk_reshaped)
    return predicted_chunk.reshape(chunk_shape[:2])

# Abra a imagem a ser classificada
image_file = askopenfilename(title='Selecione o Raster de entrada')
satellite_image = rasterio.open(image_file)

# Abra o shape com o treinamento
shapefile = gpd.read_file(askopenfilename(title='Selecione o Shape de treinamento de entrada'))

# Escolha o arquivo a ser salvo
output_file = asksaveasfilename(title='Selecione o Raster de saída')

start_time = time.time()

# Leia a imagem a converta para um array numpy
image_data = satellite_image.read()
image_data = np.moveaxis(image_data, 0, -1)
height, width, bands = image_data.shape

# Obtenha o valor dos pixels referente aos dados de treinamento
training_samples = []
training_labels = []

for index, sample in shapefile.iterrows():
    geom = sample.geometry
    coords = rasterio.transform.rowcol(satellite_image.transform, geom.centroid.x, geom.centroid.y)
    x, y = coords[1], coords[0]
    if 0 <= x < width and 0 <= y < height:
        pixel_values = image_data[y, x]
        training_samples.append(pixel_values)
        training_labels.append(sample['label'])

# Divida a amostra em teste e treino
X_train, X_test, y_train, y_test = train_test_split(training_samples, training_labels, test_size=0.2)

# Treine seu modelo
classifier = HistGradientBoostingClassifier()
classifier.fit(X_train, y_train)

# Define the number of processes (adjust as needed)
num_processes = multiprocessing.cpu_count()

# Split the image data into chunks for parallel processing
chunk_size = height // num_processes
image_data_chunks = [image_data[i:i + chunk_size] for i in range(0, height, chunk_size)]

# Initialize a multiprocessing pool
pool = multiprocessing.Pool(processes=num_processes)

# Parallelize the classification step
predicted_chunks = pool.starmap(classify_chunk, [(chunk, classifier) for chunk in image_data_chunks])

# Close the pool
pool.close()
pool.join()

# Combine the predicted chunks into the final classified image
predicted_labels = np.vstack(predicted_chunks)

# Treine o modelo com as amostras de teste
train_predictions = classifier.predict(X_test)

# Calcule a acurácia do modelo
accuracy = accuracy_score(y_test, train_predictions)

# Calcule a matriz de confusão
cm = confusion_matrix(y_test, train_predictions)

# Calcule a acurácia por classe
pa = np.diag(cm) / np.sum(cm, axis=1)

print('Confusion matrix: ')
print(cm)

print('Producer\'s accuracy:', pa)

print("Overall Accuracy:", accuracy)

# Salve a imagem
with rasterio.open(output_file, 'w', driver='GTiff', height=height, width=width, count=1, dtype=predicted_labels.dtype,
                   crs=satellite_image.crs, transform=satellite_image.transform) as output_image:
    output_image.write(predicted_labels, 1)

print("Classification completed in", time.time() - start_time, " seconds", ", classified image saved to:", output_file)